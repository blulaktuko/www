+++
title = "2022-10-11 Yendo a la isla"
author = ["Abby Henríquez Tejera"]
date = 2022-10-11T12:03:00+02:00
lastmod = 2022-10-11T12:03:00+02:00
categories = ["diary"]
draft = false
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2022-10-11T12:03:00+02:00
      note = "Esta entrada es sobre todo para probar el diario."
+++

Estaré parte de noviembre en Gran Canaria. Estaré teletrabajando, principalmente
desde Las Palmas, con lo que tengo libre tardes y findes; pero haré también
alguna escapada (puede que Tenerife y Madrid).
