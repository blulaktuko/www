;; -*- lexical-binding:t -*-

(require 's)
(require 'consult)
(require 'dash)

(load-file "macros.el")

(defun +paradoja/www-org-captures (content-path)
  (cl-labels
      ((base-template (&rest tag-list)
         (let* ((tags (if (and (listp tag-list)
                               (cl-plusp (length tag-list)))
                          (format ":%s:"
                                  (mapconcat
                                   (lambda (tag) (s-chop-prefix ":" (symbol-name tag)))
                                   tag-list ":"))
                        "")))
             (s-join
              "\n"
              `("* TODO %^{Post title}"
                ,(format  "** TODO %%\\1 %s" tags)
                ":PROPERTIES:"
                ":EXPORT_FILE_NAME: %^{Filename in English/source}.%^{Language|en|es}.md"
                ":EXPORT_LANGUAGE: %\\3"
                ":END:"
                "%?"))))
       (diary-template ()               ; has side effects
         (let ((date (org-read-date)))
           (s-join
            "\n"
            (list
             (format  "* TODO %s %%^{Post title}" date)
             (format  "** TODO %s %%\\1" date)
             ":PROPERTIES:"
             (format
              ":EXPORT_FILE_NAME: %s-%%^{Filename diary_%s_}.%%^{Language|en|es}.md"
              date date)
             ":EXPORT_LANGUAGE: %\\3"
             ":END:"
             "%?"))))
       (icon-archive-link-template
        ()
        (s-join
         "\n"
         (list
          "#+ATTR_HTML: :title %^{Archive link title (X archives)}"
          "#+begin_archive-icon"
          "[[id:%c][{{{archive}}}]]"
          "#+end_archive-icon"
          "%^{Section description}")))
       (get-header-current-file (file-path)
         (let* ((options
                 (save-window-excursion  ; if the file is not visited,
                   (find-file file-path) ; the capture sometimes fails
                   (org-map-entries      ; when the buffer is visited
                    (lambda ()
                      (let* ((title-given
                              ;; Sometimes (I think for unsaved? new headings)
                              ;; `org-element-property` returns a list
                              (org-element-property
                               :title (org-element-at-point)))
                             (title (if (listp title-given)
                                        (car (flatten-list title-given))
                                      title-given)))
                        (list title (point))))
                    "LEVEL=1")))
                (selected (consult--read options
                                         :prompt "Heading: "
                                         :sort nil))
                (found (-find (lambda (elem) (string= selected (car elem)))
                              options)))
           (if found
               (goto-char (cadr found))
             (let ((first-header-position (cadar options)))
               (goto-char first-header-position)
               (insert "* " selected "\n")
               (goto-char first-header-position)))))
       (create-normal-post (key name file heading template)
         `(,key ,name
           entry
           (file+olp ,(f-join content-path
                              file)
                     ,heading)
           ,template
           :prepend t
           :jump-to-captured t))
       (create-here (key name template)
           `(,key ,name
             plain (here)
             ,template))
       (query-heading-post (key name file template)
         (let ((file-path (f-join content-path file)))
           `(,key ,name
             entry
             ;; `file+function` used here as function alone requires visiting
             ;; the file while creating the capture, changing the behaviour a
             ;; bit (the buffer shows before the capture is finished)
             (file+function ,file-path
                            (lambda () (,#'get-header-current-file ,file-path)))
             ,template
             :prepend t
             :jump-to-captured t))))
    `(("h" "Hugo WWW post")
      ,(create-normal-post "hP" "Hugo main post"
                           "main-posts.org" "Posts" (base-template))
      ,(create-normal-post "hr" "Hugo rant post"
                           "hotchpot.org" "Posts" (base-template :rant))
      ,(create-normal-post "hp" "Hugo hotchpot"
                           "hotchpot.org" "Posts" (base-template))
      ,(create-normal-post "hd" "Hugo diary post"
                           "diary.org" "Posts" `(function ,#'diary-template))
      ,(query-heading-post "hm" "Mini-reviews"
                           "mini-reviews.org" (base-template))
      ("." "Here")
      ,(create-here ".a" "Hugo archive icon link" (icon-archive-link-template)))))

;;; probably remove from here
(require 'org) ; we need to initialize org first
(defvar +paradoja/wwww-project-path (f-join (getenv "HOME") "projects/www"))
(setq org-capture-templates
      (append +paradoja/org-capture-templates
              (+paradoja/www-org-captures
               (f-join +paradoja/wwww-project-path "content-org"))))
