+++
title = "2022-10-31 Dejo el curro actual"
author = ["Abby Henríquez Tejera"]
date = 2022-10-31T17:14:00+01:00
categories = ["diary"]
draft = false
summary = "Por qué dejo el curro actual y qué viene después"
+++

<aside>

<details>
<summary>Nota</summary>
<div class="details">

El texto puede ser algo largo. Está estructurado como las tradicionales guías de estilo peridístico, con puntos más importantes antes, y detalles menos relevantes después. Se puede dejar de leer en cualquier punto que deje de ser interesante (o no leer, claro).
</div>
</details>

</aside>

<details>
<summary>Menu</summary>
<div class="details">

<div class="ox-hugo-toc toc local">

- [Situación actual y planes para luego](#situación-actual-y-planes-para-luego)
- [Por qué me voy](#por-qué-me-voy)
- [Opciones para mí en la empresa](#opciones-para-mí-en-la-empresa)

</div>
<!--endtoc-->
</div>
</details>


## Situación actual y planes para luego {#situación-actual-y-planes-para-luego}

Presenté mi renuncia en mi empresa actual. Mi último día será a finales de enero (el último día de trabajo serio será el 26 de enero, ya que el resto lo cogí de «vacaciones»). Básicamente según mi contrato, una vez presentada mi renuncia trabajo durante tres meses más contando desde el final del mes en que presento la renuncia.

Actualmente no tengo ningún plan para qué hacer después, ya veré 🤔.

-   Empiezo esta misma semana a hablar con alguna empresa para buscar otra haciendo el mismo tipo de trabajo.
-   Estoy considerando también otras opciones, como certificarme como [Técnico Superior en Dietética](<https://www.todofp.es/que-estudiar/logse/sanidad/dietetica.html>) o algo similar, lo que podría implicar un tiempo sin trabajar. Ya veré, aún hay algo de tiempo.
-   De momento tampoco tengo pensado dejar Berlín - estoy contento de momento allí -, pero como siempre, esto puede cambiar en el futuro.


## Por qué me voy {#por-qué-me-voy}

Me di cuenta que cada tarde llegaba a casa (o bueno, los días que trabajaba de casa, apagaba el ordenador) y estaba básicamente totalmente consumido mentalmente. No podía hacer apenas otra cosa, e incluso las tareas del día a día se me hacían difíciles. Los fines de semana hasta hace nada los usé principalmente para hacerme cargo de las cosas que tenía pendientes y eran urgentes, dejando muy poco espacio para cualquier tipo de vida personal (y mis amigos de Berlín ya se preguntaban si seguía vivo). Además, empecé a quedar convencido de que esto no sería una situación temporal, sino lo que me esperaba a largo plazo si me quedaba, y la verdad, esto no es una expectativa que me entusiasmara.


## Opciones para mí en la empresa {#opciones-para-mí-en-la-empresa}

Mi intención en la empresa era mejorar la cultura del grupo en el que trabajaba, y apoyar a algunos compañeros y procesos también fuera del grupo. Aunque creo que mucho del trabajo que tenía pensado está bien en marcha y avanzando, llevar el proceso a un punto en que estuviera contento requerirá tiempo y apoyo. Y de forma sencilla - no sentí este apoyo. Aunque hay muchas buenas intenciones en la empresa a todos los niveles, esto no se traduce siempre en acciones que de verdad ayuden, y el proceso de cambio ya empezaba a resultarme demasiado lento.

Además, en mi afán de mejora y cambio, algunas de las formas en que expresé que algunas cosas no iban como debieran y debían cambiar fueron tomadas de forma no muy positiva, y viendo esto y otras situaciones similares, mi impresión es que mi influencia para influir cambios es muy limitada o nula. Esta es una crítica personal a muchos sitios que veo - hay buenas intenciones, pero no se reconoce la gravedad de algunos problemas, y el no ser realmente transparente con la situación actual no permite tomar las medidas adecuadas para poder tener un cambio efectivo.

Cuando mi jefe anunció su retirada dos opciones se me vinieron a la cabeza. O alguien vendría a reemplazarle - y dudaba (y dudo aún) que su reemplazo pudiera ayudar en el corto o medio plazo debido a la complejidad de nuestro grupo -, o le reemplazaba yo - cosa que estoy convencido no pasaría en esta empresa -. El efecto es que como mínimo estaría durante años en una situación que ya empezaba a ser insostenible para mí. La única opción que empecé a considerar posible fue el irme dejando las cosas lo mejor posible para que otra gente pudiera seguir.
