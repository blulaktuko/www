;; -*- flycheck-disabled-checkers: (emacs-lisp-package emacs-lisp-checkdoc); -*-

(defvar native-comp-deferred-compilation-deny-list nil)
(defvar bootstrap-version)
(let* ((project-dir
        (substring (shell-command-to-string "git rev-parse --show-toplevel")
                   0 -1))
       (user-dir (concat project-dir
                         "/.local/_gen/emacs"))
       (bootstrap-file
        (expand-file-name "straight/repos/straight.el/bootstrap.el" user-dir))
       (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package
 '(ox-hugo :host github :repo "kaushalmodi/ox-hugo" :nonrecursive t))
(with-eval-after-load 'ox
  (require 'ox-hugo))
(require 'org)
(add-to-list 'org-modules 'org-id 'append)
(setq org-log-into-drawer "LOGBOOK")
(setq org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)

(load-file "elisp/macros.el")

;; (setq org-export-global-macros
;;       '((archive . "@@html:<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-archive\"><polyline points=\"21 8 21 21 3 21 3 8\"></polyline><rect x=\"1\" y=\"3\" width=\"22\" height=\"5\"></rect><line x1=\"10\" y1=\"12\" x2=\"14\" y2=\"12\"></line></svg>@@")))
(let ((files (directory-files-recursively "content-org/" "\\.org$")))
  (dolist (file files)
    (save-excursion
      (with-current-buffer (find-file file)
        (message (format "Processing: %s" file))
        (org-hugo-export-wim-to-md :all-subtrees nil t :noerrors)))))
