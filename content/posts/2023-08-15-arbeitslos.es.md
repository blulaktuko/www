+++
title = "2023-08-15 Seis meses en paro"
author = ["Abby Henríquez Tejera"]
date = 2023-08-14T18:02:00+02:00
categories = ["diary"]
draft = false
+++

Ha pasado un buen tiempo desde la [última actualización]({{< relref "2022-10-31-dejo-el-curro-actual.es.md" >}}). Terminé dejando mi empresa como estaba previsto.

Empecé también a estudiar para los exámenes libre de dietética, pero hasta dentro de uno o dos años, como mínimo, no habré terminado de presentarme (lo tengo aún en marcha). Y desde que dejé la empresa -- y de antes de hacerlo --, he hablado con algunas empresas, pero ninguna ha terminado cuajando -- bueno, y en la última me rechazaron, pero así es la vida --.

Los primeros meses tras dejar el trabajo se perdieron en descansar y desconectar. No pensaba realmente que hiciera falta tanto tiempo, pero se ve que no me lo tomé lo en serio que hubiera debido ;) . Ya después me puse a estudiar alemán. El objetivo es acabar hablando de forma fluida, y aparte de apuntarme en clases (elegí apuntarme a un curso de nivel B2[^fn:1]), he estado trabajando bastante en otras formas de trabajar el idioma: viendo series, leyendo, tanto libros como _online_, y cosas así. Pondré más detalle en otra entrada.

Actualmente estoy considerando hacer una formación adicional profesional por medio de la agencia de desempleo. Es una idea que me dieron ellos, me llamaron y sugirieron buscar un curso que ellos pagarían. He hablado con una empresa que los ofrece y parece que por ahí va bien. A pesar de no cumplir los requisitos oficiales de los cursos que miré, ya que no tengo un título que presentar, y es algo que normalmente piden, se ve que están contentos con recibir nuevos alumnos si estos parecen demostrar que pueden sacar provecho de los estudios. Mi principal[^fn:3] preocupación era precisamente el alemán, pero también la mayor motivación: haciendo el curso me fuerzo aún más a tener que interactuar en el idioma en un entorno algo más natural, durante horas y sin vía de escape. Lo que me queda ahora es hablar con la agencia de desempleo para que aprueben el curso específico que termine eligiendo. Aunque tras ver los precios (decenas de miles de euros), pues no sé cómo irá la cosa, me parece un poco disparate.

[^fn:1]: Esta es otra historia. En el paro alemán hice hace unos años un examen de nivel y me [salió C1](https://es.wikipedia.org/wiki/Marco_Com%C3%BAn_Europeo_de_Referencia_para_las_lenguas)[^fn:2]..., pero eso claramente no es correcto. Así que fue un poco de jaleo _bajar_ el nivel.
[^fn:2]: El examen de nivel era solo leer textos cortos y responder preguntas tipo test, cosa que supongo se me da especialmente bien. O ese día me tomé un café excepcional o algo.
[^fn:3]: Pero claro, no única. Los contenidos del curso también son interesantes e útiles para mí. No comento todavía mucho ya que hay unas cuantas opciones que he hablado con la empresa y aún no estoy seguro cómo irá la cosa, si va al final.
