# Blulaktuko's web
#
# @file
# @version 0.1
.PHONY: clean build build-all setup hugify server deploy

build:
	mkdir -p .local/_gen/emacs/straight/repos/straight.el/
	emacs --batch -l elisp/batch-emacs.el

build-all: clean build hugify

clean:
	rm -rf public/* content/*

setup:
	@mkdir -p .local/_gen/vscode_eslint
	@wget "https://github.com/emacs-lsp/lsp-server-binaries/blob/master/dbaeumer.vscode-eslint-2.2.2.vsix?raw=true" -O .local/_gen/vscode_eslint/server.zip
	@unzip -f .local/_gen/vscode_eslint/server.zip -d resources/_gen/vscode_eslint
	@rm .local/_gen/vscode_eslint/server.zip

hugify:
	hugo --gc --minify

server:
	hugo server --buildDrafts

update: # theme
	hugo mod get -u

deploy: hugify
# end
