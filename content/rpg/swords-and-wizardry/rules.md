+++
title = "S&W rule choices and home rules"
author = ["Abby Henríquez Tejera"]
categories = ["swords-wizardry"]
draft = false
+++

When playing games of S&amp;W, assume the following rules (or rule choices) in most games. Some rules may depend on the specific setting and are not shown here.


## Character creation {#character-creation}


### Attributes {#attributes}

-   Roll 3d6 in order per attribute (Str, Dex, Con, Int, Wis, Cha)
-   If there are more than 3 results under 9, or the average is less than 9, all the attributes can be rerolled (again, in order).
-   Choose either:
    -   swap if you want two attribute values you choose; add +1 to one of the attributes of your choice
    -   replace an attribute of your choice by 15


### HP {#hp}

At first level, if the rolled hit points are less than half the possible hp after Con adjustment (rounding up), use half the possible hp.

> Eg., a recently created cleric (level 1) with Con 13 (+1 hps per level, so 1d6+1 HD) rolls a 2 in the die when rolling hit points. That would be 2+1 = 3 hit points.
>
> Half the maximum value, rounding up is ⎡0.5\*(6+1)⎤ = 4.
>
> As 3 &lt; 4, the cleric will have 4 hit points.

If a character is created directly with level higher than 1, this minimum is calculated the previously mentioned way, and then the rest of the HD can be rolled together.


## AC {#ac}

Use ascending AC.


## Attack bonus {#attack-bonus}

Only fighters get bonus to hit from Str., and they get it also for missile weapons.


## Class-specific {#class-specific}


### Cleric {#cleric}

-   First level clerics with Wisdom &gt; 15 have an additional 1st. level spell.
-   Clerics can use slings.


## Combat {#combat}

-   Morale rules are used (B/X style)
