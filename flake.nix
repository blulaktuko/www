{
  description = "Personal web page";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let inherit (nixpkgs.lib) optional optionals;

        in pkgs.mkShell {
          buildInputs = with pkgs; [
            go # _1_21
            hugo
            gnumake
            nodejs
            nodePackages.eslint
            wget
          ];
          shellHook = "";
        };
      });
}
