+++
title = "El rey de hierro"
author = ["Abby Henríquez Tejera"]
date = 2023-04-12T09:51:00+02:00
tags = ["books", "books the-accursed-kings", "historical-fiction", "fiction", "medieval-history", "french-literature", "author maurice-druon"]
categories = ["reviews", "book"]
draft = false
summary = "Book review"
+++

{{< bookinfo goodreads="https://www.goodreads.com/book/show/99523.El_rey_de_hierro" publisher="Javier Vergara Editor" publication-date="1981 (orig. en francés 1965)" edition="Bolsillo" pages="362" rating="5/5" series-link="/es/tags/books-the-accursed-kings" series="Los reyes malditos" cover-art="Verónica López" original-language="francés" original-title="Le Roi du Fer" translator="Raimundo Oliaga Rincón" author="Maurice Druon" >}}
El rey de hierro
{{< /bookinfo >}}

La serie de _Los reyes malditos_ es conocida hoy día fuera de Francia principalmente debido a los [elogios](https://www.theguardian.com/books/2013/apr/05/maurice-druon-george-rr-martin) de [George R. R. Martin](https://es.wikipedia.org/wiki/George_R._R._Martin). Antes de eso ya gozaba de bastante salud, lo que se puede ver por las múltiples ediciones que el libro ha tenido en español (se puede consultar la página de Goodreads, donde por ejemplo ni siquiera sale la edición que yo tuve en mis manos).

Este libro fue recomendación (y préstamo) de un muy buen amigo, Nacho, que ya hace tiempo, ¿décadas?, me recomendó bastante leerlo. Ahora me siento mal por haber tardado tanto en empezar a leerlo.

La serie cuenta la historia de la [guerra de los 100 años](https://es.wikipedia.org/wiki/Guerra_de_los_Cien_A%C3%B1os), entre las coronas de Francia e Inglaterra, en el siglo XIV. Empieza en 1314, con el final del proceso que dio fin a la Orden del Temple, y sigue a partir de ahí. Los protagonistas del libro son realeza, nobleza y otra gente importante, por ejemplo banqueros _lombardos_, de la época. De muchas formas es una historia familiar --- en el sentido de que muchos de los personajes son familia ---, y también hay muchos personajes, con lo que al principio me vi tomando notas para recordar quién es quién y qué relación tenían. Después de unos pocos capítulos, de todas formas, uno ya se acostumbra y no cuesta seguir qué pasa.

Hay que, eso sí, a veces revisar las notas biográficas de los personajes al final del libro[^fn:1]. Estas notas a veces revelan qué pasará con alguno de ellos, pero realmente esto no resta a la historia.

Hablando de notas, el libro incluye unas pocas notas históricas explicando cosas del periodo. Algunas reseñas que leí se quejan de que el libro se lee mucho como un libro de texto de historia y poco como novela. Entiendo por qué alguien podría decir esto, aunque estas notas son breves y no demasiadas, y para mí aportan y ayudan bastante. Además, la historia en sí es lo mejor del libro, es difícil superar la tensión e intriga que ocurren. Olvidando la parte histórica, se puede leer perfectamente como entretenimiento. Por otro lado, leerlo para entender mejor los motivos de la guerra o el periodo histórico es algo que sí creo que pueda funcionar muy bien. Intentar entender a veces los motivos del pasado, cuando estos son muy complejos y hay muchas cosas interrelacionadas, puede ser difícil, pero Druon logra contar muchas historias paralelas y relacionadas de buena forma.

Del estilo de narración Druon (y el traductor), hacen buen trabajo. El libro se lee bien, y la mayoría de términos en desuso hoy día se explican o se pueden entender por contexto (aprendí por ejemplo qué era una [escarcela](https://dle.rae.es/escarcela)). En alguna ocasión leyendo me pregunté por qué la historia seguía a algún personaje haciendo algo que en otras novelas se habría saltado o explicado luego en unas líneas, pero nunca aburre y al final todo lo escrito tiene un motivo.

Los libros siguientes ya los pedí, de mi [proveedor de libros de segunda mano favorito](https://www.abebooks.com/servlet/SearchResults?bi=0&cm_sp=SearchF-_-sf-_-Results&ds=30&sortby=17&sts=t&vci=63730288) y los espero impaciente.

{{< figure src="/images/books/el-rey-de-hierro.jpg" >}}

[^fn:1]: Estos libros probablemente merezca la pena por tanto leerlos en físico mejor que en un lector digital actual, en los que ir a las notas del final y volver es más tedioso o complicado.
