+++
title = "Privacidad y uso de datos"
author = ["Abby Henríquez Tejera"]
date = 2023-02-06T23:25:00+01:00
categories = ["pages"]
draft = false
hidden = true
+++

Aquí no se recogen ningún tipo de datos direcdtamente (sí, nada de _tracking_, analítica o cosas similares).

Como proveedor de alojamiento, [Netlify](https://www.netlify.com/) tiene acceso a algo de infromación que ellos guardan. No es demasiada y se puede revisar en sus páginas de [privacidad](https://www.netlify.com/privacy/) y del [RGPD (GDPR)](https://www.netlify.com/gdpr-ccpa), ambas en inglés. En resumen mío (que no soy abogado, ojo), guardan _logs_ de acceso (qué páginas visita una IP específica y cuándo).
