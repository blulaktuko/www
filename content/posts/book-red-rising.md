+++
title = "Red Rising"
author = ["Abby Henríquez Tejera"]
date = 2023-04-11T17:25:00+02:00
tags = ["books", "books red-rising-saga", "science-fiction", "young-adult", "fiction", "dystopia", "american-literature", "english-speaking-literature", "author pierce-brown"]
categories = ["reviews", "book"]
draft = false
summary = "Book review"
+++

{{< bookinfo goodreads="https://www.goodreads.com/book/show/15839976-red-rising" pages="382" series-link="/tags/books-red-rising-saga" series="Red Rising Saga" rating="3½/5" author="Pierce Brown" >}}
Red Rising
{{< /bookinfo >}}

I saw _Red Rising_ being recommended a lot in online forums, almost always with great praise. Before starting to read it, as I sometimes do, I tried to check some of the existing reviews (hopefully without spoilers) to see if it was something I was in the mood for next. A big amount of them mentioned that this book was not at all a young adult novel, that comparisons with the _Hunger Games_ where unmerited and other similar opinions. I'm just surprised someone can believe this honestly.

When I started reading, for about the first fifth of the book, the story seemed bland and cheap, trying too hard to elicit emotion and not really succeeding. It pretty much hit all of the tropes of young adult novels I could think of --- and I certainly could see how someone would compare it with the _Hunger Games_ ---. I did consider about whether I wanted to continue reading, but I realised that it was an easy read and the world itself wasn't uninteresting. If I had to rate it then I'd go without doubt for a `1/5`.

But then things got really interesting.

The tropes where still there. Some things of the end of the book where clear --- the genre requires them ---, and eventually they where there. And while the impression of a thin plot trying very hard to make me feel emotions... well, sometimes it worked, and there is certainly a couple of surprises. But more importantly, I could not let the book lie down for a minute if I could help it. It's been a long time since a book hooked me up so much. That it was with a book that from the first moments had caused, well, quite a negative impression I guess caught me by surprise.

The book is not the _Hunger Games_, but the comparison is a good one to see if the book can be recommended to someone else. It's young adult science fiction in a dystopia with a young main character that accomplishes more than could be expected at first. I'd say the book itself accomplishes also more than it could be expected at first. I won't say it's one of the greatest novels of the XXI century, but for sure I'm happy of having read it and do want to continue reading the series.
