+++
title = """
  Hostile Anti-User Practice: Reddit's image "viewer"
  """
author = ["Abby Henríquez Tejera"]
date = 2023-08-14T20:22:00+02:00
tags = ["rant", "hostile-antiuser-practice"]
categories = ["hotchpot"]
draft = false
+++

Since a few months ago Reddit has decided that they want to control your actions/annoy/control your attention[^fn:1] even more. If you try to open an image hosted with them, the image itself won't just open, but a full web page will open with styled header and footer.

That is, if you go to a Reddit photo link like [`https://i.redd.it/vchz7tqd61ib1.png`](https://i.redd.it/vchz7tqd61ib1.png) (this is a real link, if you click it you can experience what I explain in the post, but also will be _registered_ by Reddit[^fn:2]), instead of loading the photo itself, which is how browsers/internet would normally work, it redirects to another page that has the photo in the middle.

<aside>

{{< figure src="/images/hotchpot/reddit-image-viewer.png" alt="Portada del libro" class="in-text" >}}

</aside>

Apart from that itself is quite hostile and a bad practice, as it usually happens, this also has consequences. If you want to open the image to check on it, zoom on it to a specific level (but not full zoom) or other things, you may be hindered or it may be impossible.

If you use Firefox there's [an extension that may help](https://addons.mozilla.org/en-US/firefox/addon/load-reddit-images-directly/). For Chrome, there's [apparently another one](https://chrome.google.com/webstore/detail/reddit-load-images-direct/fpimmmjbglpnlpbfikgekaaeinminolo), although I don't know if that works with other Chromium-based browsers.

[^fn:1]: Choose 3 of the options.
[^fn:2]: To add to the complains in this article, in some situations the page that opens will say that the image is deleted, even if it's not.
