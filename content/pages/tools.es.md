+++
title = "Herramientas"
author = ["Abby Henríquez Tejera"]
date = 2023-02-06T23:22:00+01:00
categories = ["pages"]
draft = false
hidden = true
+++

Esta web está alojada en [Netlify](https://netlify.com), y el código lo puedes encontrar en [Gitlab](https://gitlab.com/blulaktuko/www).

Está escrita usando [Hugo](https://gohugo.io/), [PaperMod Theme](https://github.com/adityatelange/hugo-PaperMod), [Doom](https://github.com/hlissner/doom-emacs) [Emacs](https://www.gnu.org/software/emacs/) y entre otros [ox-hugo](https://github.com/kaushalmodi/ox-hugo) (y, por supuesto, [org-mode](https://orgmode.org/)).
