+++
title = "OSE rule choices and home rules"
author = ["Abby Henríquez Tejera"]
categories = ["ose"]
draft = false
+++

When playing games of OSE, assume the following rules (or rule choices) in most games. Some rules may depend on the specific setting and are not shown here, on top of other variations (eg. classes used or whether Advanced optional rules are used or not).


## From Core Books {#from-core-books}

-   Ascending AC
-   Reroll 1s and 2s on level 1 hp
-   Variable weapon damage
-   Subduing (dmg) is an option
-   Morale is used


## Carcass Crawler 1 {#carcass-crawler-1}

-   D6 Thief Skills (also check guidelines for adjudicating thief skills)


## Carcass Crawler 2 {#carcass-crawler-2}

-   Assume Town Services are available (in appropriate towns, of course)
-   Hiring Retainer rules can be used
-   Quick Equipment rules can be used
-   Item-Based Encumbrance is used (with STR modifier)


## Carcass Crawler 3 (or 0, inaugural issue) {#carcass-crawler-3--or-0-inaugural-issue}

-   Expanded equipment can be assumed to be used.
