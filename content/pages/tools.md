+++
title = "Tools"
author = ["Abby Henríquez Tejera"]
date = 2022-10-08T01:52:00+02:00
categories = ["pages"]
draft = false
hidden = true
+++

This site itself is hosted with [Netlify](https://netlify.com), whereas the code repo is on [Gitlab](https://gitlab.com/blulaktuko/www).

It's written and built using [Hugo](https://gohugo.io/), [PaperMod Theme](https://github.com/adityatelange/hugo-PaperMod), [Doom](https://github.com/hlissner/doom-emacs) [Emacs](https://www.gnu.org/software/emacs/) and among others [ox-hugo](https://github.com/kaushalmodi/ox-hugo) (and [org-mode](https://orgmode.org/) of course).
