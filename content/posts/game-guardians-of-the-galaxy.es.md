+++
title = "Guardianes de la Galaxia"
author = ["Abby Henríquez Tejera"]
date = 2023-08-21T21:19:00+02:00
categories = ["reviews"]
draft = false
+++

{{< gameinfo steam="https://store.steampowered.com/app/1088850/Marvels_Guardians_of_the_Galaxy/" hltb="https://howlongtobeat.com/game/72584" status-played="19 h.; completado" metacritic="https://www.metacritic.com/game/playstation-5/marvels-guardians-of-the-galaxy" rating="3,5 o 4/5 (aún decidiéndome ;) )" platform-played="PC (Steam)" release-date="2021" publisher="Eidos Interactive" developer="Eidos Montréal" >}}
Guardianes de la galaxia / <i>Marvel's Guardians of the Galaxy</i>
{{< /gameinfo >}}

Estos días decidí probar el juego de los Guardianes de la Galaxia. El juego se juega en tercera persona, y controlamos un personaje (_StarLord_ o Quill) todo el rato. Por medio de un menú, podemos pedirle al resto del equipo (fundamentalmente los mismos personajes que en la primera película) que actúen, ya sea en combate o para resolver puzles. Ocasionalmente hay partes de minijuegos (controlando la nave por ejemplo) pero son partes pequeñas. También hay hay unos cuantos «eventos de tiempo rápido» (_quick time events_), aunque la mayoría no son esenciales y los pocos que son se pueden repetir; en la dificultad normal, hay bastante tiempo de sobra para darle al botón requerido. La historia y el desarrollo es muy lineal, pero permite algo de exploración y no termina volviéndose aburrido, aunque a veces sí que me dio la impresión de ser una película muy interactiva.

{{< figure src="/ox-hugo/gameplay.jpg" caption="<span class=\"figure-number\">Figura 1: </span>Imagen de juego ([tomada de la Wikipedia](https://en.wikipedia.org/wiki/File:Guardians_of_the_Galaxy_gameplay_screenshot.jpg))" >}}

El mundo de juego no es el de las películas del mismo nombre, y que haya visto no es el de ningún cómic en particular, sino que es un nuevo universo, pero la historia pega mucho con el equipo. Los personajes, como dije, son los de la primera película (Quill, Rocket / Mapache Cohete, Groot y Drax el destructor), pero no debemos esperar que sean iguales que en la película. El pasado es ligeramente distinto para cada uno de ellos, y las relaciones entre ellos también son distintas. Aún así, el espíritu del equipo se mantiene y uno de los puntos fuertes del juego son los diálogos. Podemos encontrar cosas durante el juego que abren diálogos, y la verdad es que son interesantes y profundizan en el pasado y las motivaciones de los personajes. Pero es que además, durante los momentos de acción en sí los diálogos están genial logrados, además de que cada personaje se va comportando acorde a su personalidad (en combate, resolviendo puzles y en general siempre). Durante encontramos otros personajes, algunos icónicos, que también se integran muy bien en la historia y en los diálogos, y hay un muy buen desarrollo de las relaciones de los personajes. Casi que diría que el juego es de mis historias de los guardianes favoritas (o mi peli suya favorita).

{{< figure src="/ox-hugo/cabina.jpg" caption="<span class=\"figure-number\">Figura 2: </span>Discutiendo con el equipo en la cabina de la Milano" >}}

Jugué en inglés, así que no sé cómo está doblado al español, pero si los actores de doblaje y los diálogos están la mitad de bien conseguidos que en inglés, deben de estar entonces también muy bien logrados.

{{< figure src="/ox-hugo/naves.jpg" >}}

La dificultad no es muy alta, en el modo de dificultad normal, aunque a veces sorprende. Aún así, cada parte se puede volver a repetir sin problema, así que al final es una experiencia interactiva que creo que en general se puede disfrutar mucho.

Así pues, no puedo más que recomendar el juego, especialmente si uno a uno le gustan los guardianes.
