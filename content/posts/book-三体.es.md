+++
title = "El problema de los tres cuerpos"
author = ["Abby Henríquez Tejera"]
date = 2023-05-11T15:02:00+02:00
tags = ["books", "books remembrance-of-earths-past", "science-fiction", "fiction", "chinese-literature", "author liu-cixin"]
categories = ["reviews", "book"]
draft = false
summary = "Book review"
+++

{{< bookinfo goodreads="https://www.goodreads.com/book/show/28487182-el-problema-de-los-tres-cuerpos" publisher="Ediciones B (Penguin Random House)" publication-date="2016 (orig. en mandarín 2008)" edition="Digital" pages="416" rating="4,5/5" series-link="/es/tags/books-remembrance-of-earths-past" series="El problema de los tres cuerpos" original-language="mandarín" original-title="三体" translator="Javier Altayó" author="Liu Cixin (刘慈欣)" >}}
El problema de los tres cuerpos
{{< /bookinfo >}}

<aside>

{{< figure src="/images/books/el-problema-de-los-tres-cuerpos/el-problema-de-los-tres-cuerpos.jpg" alt="Portada del libro" class="in-text" >}}

</aside>

_El problema de los tres cuerpos_ lo leí por primera vez hace unos 8 o 9 años. Me causó entonces una gran impresión y no podía esperar a terminar de leer los siguientes libros. Como me suele pasar, terminé olvidándome de ellos 😅 . Recientemente un buen amigo empezó a leerlos y quería animarme a terminármelos, pero me di cuenta que, también como me suele pasar, apenas me acordaba. Así que me lo decidí leer de nuevo.

Del libro poco recordaba, pero mi opinión sí la tenía más clara, y la experiencia parece que fue muy similar aquí. Las ideas del libro son geniales y es difícil soltarlo. La historia tiene fuerza básicamente por lo que vamos descubriendo y es el ansia de descubrir más lo que empuja a uno a seguir leyendo, e incluso a disfrutar del libro.

Por otro lado, hay poco desarollo de la mayoría de los personajes. Salvo quizá uno de ellos, {{< inlinespoiler >}}Ye Wenjie{{< /inlinespoiler >}}. El principal protagonista tiene una vida, una familia, pero podría igualmente no tenerla y cambiaría solo unas pocas líneas del texto. No es un libro en el que los personajes lleven el peso de la acción en sí, ni tampoco uno que se lea por la belleza de la escritura (si bien esto puede deberse en parte a la traducción, creo que la primera vez leí la traducción al inglés y tuve una opinión similar). No es que esté mal escrito, es que lo importante es lo que ocurre y lo que vamos descubriendo.

El paso en el que se descubren cosas está muy logrado, y eso es lo que termina dejando muy buen sabor de boca (a mí al menos) y lo que más impulsa a no parar de leer. Aunque el libro tiene un poco del síndrome de «esto que te contamos realmente no es tan complicado, pero te lo presentamos como si fuera lo más complicado y a la vez transcendental del mundo», tampoco termina dando un aire presuntuoso.

Un pequeño problema para algunos lectores occidentales puede ser recordar los nombres de los personajes (ya que uno puede no estar acostumbrado a los nombres chinos - y en particular a cosas como el apellido se diga antes por ejemplo -). También el libro navega parte de la historia china, pero no es requisito saber del tema para no perderse algo. Sobre historia china, la verdad, la primera vez me sorprendió que un libro chino pudiera dar la apariencia de ser tan crítico con la revolución cultural china (los primeros capítulos la muestran de forma cruda), pero supongo que es un episodio _superado_ ya incluso en China.

A pesar de no ser un libro _literario_ ni de entrar en el panteón de cosas de disfrutar solo por la belleza de sus párrafos, para alguien a quien la ciencia ficción (o leer de ciencia en sí) le guste.
