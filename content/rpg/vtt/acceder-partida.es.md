+++
title = "Acceso a las partidas"
author = ["Abby Henríquez Tejera"]
date = 2022-10-09T00:59:00+02:00
draft = false
+++

1.  Ir al servidor y si hace falta poner la clave de administración del servidor.
    Ambos se pueden encontrar en el drive <https://drive.google.com/drive/folders/1gAVAq9n_mG9_2B43R5MtDeHI_EUc0Q5e?usp=sharing> .
2.  Si la partida no está empezada, darle a `Launch World`.
3.  Loguearse como el usuario que uno tenga.
4.  ...
5.  Profit! (Hacer lo que uno deba)

Posteriormente estaría bien «cerrar el mundo»:

1.  En la partida, ir a la última pestaña del menú de la derecha.
2.  Ahí darle a `Return to setup` (si está disponible).
