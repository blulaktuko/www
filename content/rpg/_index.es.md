+++
title = "JdR"
author = ["Abby Henríquez Tejera"]
date = 2022-10-09T00:54:00+02:00
draft = false
+++

<div title="Archivos de JdR" class="archive-icon">

[<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-archive"><polyline points="21 8 21 21 3 21 3 8"></polyline><rect x="1" y="3" width="22" height="5"></rect><line x1="10" y1="12" x2="14" y2="12"></line></svg>]({{< relref "archives.es.md" >}})

</div>

Cosas sobre JdR que voy jugando o en los que estoy interesado.
