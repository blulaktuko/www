+++
title = "El bosque oscuro"
author = ["Abby Henríquez Tejera"]
date = 2023-06-05T20:53:00+02:00
tags = ["books", "books remembrance-of-earths-past", "science-fiction", "fiction", "chinese-literature", "author liu-cixin"]
categories = ["reviews", "book"]
draft = false
summary = "Book review"
+++

{{< bookinfo goodreads="https://www.goodreads.com/book/show/32827084-el-bosque-oscuro" publisher="Ediciones B (Penguin Random House)" publication-date="2017 (orig. en mandarín 2008)" edition="Digital" pages="576" rating="3,5/5" series-link="/es/tags/books-remembrance-of-earths-past" series="El problema de los tres cuerpos" original-language="mandarín" original-title="黑暗森林" translator="Javier Altayó y Jianguo Feng" author="Liu Cixin (刘慈欣)" >}}
El bosque oscuro
{{< /bookinfo >}}

<aside>

{{< figure src="/images/books/el-problema-de-los-tres-cuerpos/el-bosque-oscuro.jpg" alt="Portada del libro" class="in-text" >}}

</aside>

_El bosque oscuro_ sigue básicamente la historia donde [_El problema de los tres cuerpos_]({{< relref "book-三体.es.md" >}}) la deja. A pesar de esto, de algunas formas es un libro muy distinto.

Donde en el primero los personajes tenían poca importancia, aquí de repente alguno de los personajes son centrales {{< inlinespoiler >}}Luo Ji, por supuesto, pero se puede argumentar que todos los vallados como mínimo{{< /inlinespoiler >}}. La historia se desarrolla de una forma muy distinta. El primer libro tiene dos (o tres) partes: parte contando una historia en el pasado, que explica la situación del presente, y otra parte que ocurre en el presente y en donde realmente no ocurre demasiado (y una parte en un videojuego virtual). La falta de mucha acción, o, en general, historia, no es problema ya que lo que ocurre sirve para ir desarrollando una serie de ideas, y lo hace bien. El segundo libro, por otro lado, es todo acción. Tanta que a veces uno se pregunta si hace falta ir por todos estas escenas.

Si bien la historia se puede contar de forma más breve, en general funciona bien. De forma limitada, es una novela policíaca en la que se recibe una serie de hechos, y de ellos hay que intentar pensar quién es el asesino... o qué estrategias los caracteres van a seguir. No llega, en mi opinión, a la altura de buenos autores de novela policíaca / de detectives pero queda bien para una continuación de _El problema de los tres cuerpos_. El libro consigue evitar problemas de otras segundas partes, como repetir la historia de la primera -- desde luego no --, o ser simplemente una novela de paso. La serie de libros podría perfectamente haber acabado con el final de este libro con pocos cambios o incluso ninguno, y no quedaría tan mal.

El libro me gustó, aunque me costó más leerlo que el primero y parte de la magia falta. Aún así, a pesar de que parte de lo que hace el protagonista y otros personajes me causa rechazo, al final los personajes terminan resultando interesantes y el argumento logra ser emocionante.
