+++
title = "2022-10-31 I'm leaving my current job"
author = ["Abby Henríquez Tejera"]
date = 2022-10-31T17:14:00+01:00
categories = ["diary"]
draft = false
summary = "Why I'm leaving my current job and what comes later"
+++

<aside>

<details>
<summary>Note</summary>
<div class="details">

The current text may be a bit too long. It's structured like traditional newspaper style guides, with more important points stated earlier and less relevant details left towards the end. One can stop reading anytime (if at all!).
</div>
</details>

</aside>

<details>
<summary>Contents</summary>
<div class="details">

<div class="ox-hugo-toc toc local">

- [Current situation and plans for later](#current-situation-and-plans-for-later)
- [Why I'm leaving](#why-i-m-leaving)
- [Feelings in the company](#feelings-in-the-company)

</div>
<!--endtoc-->
</div>
</details>


## Current situation and plans for later {#current-situation-and-plans-for-later}

I presented my resignation in my current company. My last day will be at the end of January, the last real working day will be 26 January, the next days being vacation days. According to my contract, once I resign I work 3 months counting from the end of the current month.

Currently I have no plans on what to do after my last day 🤔.

-   I'll start this week talking with other companies to look for somewhere in which to continue working on something similar.
-   I'm also considering other options, such as certifying as a dietitian (in Spain), which would probably mean being some time without working. Let's see, there's still time to decide.
-   I'm staying in Berlin for now, I'm happy here (but as always, this may change at some point).


## Why I'm leaving {#why-i-m-leaving}

I realised that I was reaching every evening really exhausted. I could barely do anything else, and I felt completely mentally drained. I could not do any other thing and even daily activities and chores started to be very difficult to do. Weekends were basically catch-up periods, and left very little space for any kind of personal life. Some of my Berlin friends started wondering whether I was still alive.

On top of this I started becoming convinced that this would not be a temporary situation, but that it would be a constant for a long time if I stayed. To be honest, this was not something I was looking towards.


## Feelings in the company {#feelings-in-the-company}

My personal goal in the company was to help improve my group's culture, and support some colleagues and processes out of the group. Although lots of the work I had planned for the group is already underway, reaching a point in which I felt satisfied would require lots of time and support. Long story short - I didn't felt I could have that support. Although there's lots of good intentions in the company at all levels, this doesn't always translate into real actions, and the change process has been way too slow.

On top of this, as part of my efforts, some ways in which I have expressed myself about things not going the way I thought they should where taken in a not very positive way. Seeing this and other similar situations my impression is that my real influence is limited at most. This is a criticism I have on many organisations - good intentions abound, but the gravity of some problems is not recognised. Not being really transparent does not help when trying to push for proper measures that can bring positive change.

When my boss announced that they where leaving, two possibilities sprang to my mind: either someone else would come as a replacement (and then I could not expect any kind of support from the newcomer to the group for a long time, given the complexity of our domain), or I could replace my boss (which I don't think could happen here). The effect is that I'd be for years in a situation that started to become unbearable for me. The only option I started seeing realistic was quitting, leaving things behind as well as possible so that other people could take the baton.
