+++
title = "El fin de la muerte"
author = ["Abby Henríquez Tejera"]
tags = ["books", "books remembrance-of-earths-past", "science-fiction", "fiction", "chinese-literature", "author liu-cixin"]
categories = ["reviews", "book"]
draft = true
summary = "Book review"
+++

{{< bookinfo goodreads="https://www.goodreads.com/book/show/32827109-el-fin-de-la-muerte" publisher="Nova (Penguin Random House)" publication-date="2018 (orig. en mandarín 2010)" edition="Digital" pages="734" rating="3,5/5" series-link="/es/tags/books-remembrance-of-earths-past" series="El problema de los tres cuerpos" original-language="mandarín" original-title="死神永生" translator="Agustín Alepuz Morales" author="Liu Cixin (刘慈欣)" >}}
El fin de la muerte
{{< /bookinfo >}}

...
