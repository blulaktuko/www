;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((eval . (let* ((eslint-path (executable-find "eslint"))
                        (npm-path (executable-find "npm"))
                        (node-path (executable-find "node"))
                        (project-path (file-name-directory
                                       (let ((d (dir-locals-find-file ".git")))
                                         (if (stringp d) d (car d)))))
                        (tmp-path (f-join project-path ".local/_gen"))
                        (captures-file (f-join project-path "elisp" "org-captures.el")))
                   (setq flycheck-javascript-eslint-executable eslint-path
                         lsp-eslint-unzipped-path eslint-path
                         lsp-eslint-runtime npm-path
                         lsp-eslint-node node-path
                         lsp-eslint-server-command `(,node-path
                                                     ,(f-join tmp-path
                                                              "vscode_eslint/extension/server/out/eslintServer.js")
                                                     "--stdio"))
                   ;; (require 'org) ; we need to initialize org first
                   ;; (when (file-exists-p captures-file)
                   ;;   (load captures-file)    ; has +paradoja/www-org-captures
                   ;;   (setq-local org-capture-templates
                   ;;               (append +paradoja/org-capture-templates
                   ;;                       (+paradoja/www-org-captures
                   ;;                        (f-join project-path "content-org")))))
                   ))))
 (web-mode . ((+format-on-save-enabled-modes . (not web-mode))))
 (org-mode . ((eval . (setq org-hugo-base-dir (projectile-project-root)))
              (org-footnote-define-inline . t)))
 (js2 . ((js2-basic-offset . 4)))
 ;; ("content-org/"
 ;;  . ((org-mode . ((eval . (org-hugo-auto-export-mode))))))
 )
