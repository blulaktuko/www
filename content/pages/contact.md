+++
title = "Contact"
author = ["Abby Henríquez Tejera"]
date = 2022-10-09T03:11:00+02:00
lastmod = 2023-08-15T09:25:00+02:00
categories = ["pages"]
draft = false
hidden = true
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2023-08-15T09:25:00+02:00
      note = "Added Mastodon account &amp; removed Twitter/X account."
+++

You can contact me through the following possible places/means:

-   [abby@blulaktuko.net](mailto:abby@blulaktuko.net)
-   [github](https://github.com/paradoja/)
-   [gitlab](https://gitlab.com/paradoja/)
-   [mastodon](https://mastodon.social/@paradoja)
-   [linkedin](https://www.linkedin.com/in/abbyhenriquez/)
