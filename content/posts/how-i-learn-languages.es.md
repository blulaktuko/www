+++
title = "¿Cómo estudio una lengua?"
author = ["Abby Henríquez Tejera"]
date = 2023-08-17T12:12:00+02:00
draft = false
+++

<details>
<summary>Contenido</summary>
<div class="details">

<div class="ox-hugo-toc toc local">

- [Introducción](#introducción)
- [Repaso espaciado](#repaso-espaciado)
- [Consumición libre](#consumición-libre)
- [Consumición «controlada»](#consumición-controlada)
- [Producción](#producción)
- [Ayudas adicionales](#ayudas-adicionales)
- [Cómo empezar](#cómo-empezar)

</div>
<!--endtoc-->
</div>
</details>


## Introducción {#introducción}

Llevo un tiempo que mi principal ocupación ha sido estudiar alemán. Aparte le he echado un buen tiempo a intentar descubrir cómo aprender alemán... o en general, cómo aprender nuevas lenguas. Aunque el tema de adquisición de una lengua extranjera es fascinante, también es tremendamente amplio y no me veo con la confianza de intentar explicarlo. Por otro lado, sí que me siento cómo explicando cómo estudio _yo_ lenguas (alemán y portugués principalmente por ahora).

Lo primero es indicar que todo esto lo hago con lenguas con las que tengo cierta idea y comodidad (a partir de [B1 o al menos A2 alto](https://es.wikipedia.org/wiki/Marco_Com%C3%BAn_Europeo_de_Referencia_para_las_lenguas)). Al final pondré alguna indicación de cómo ir con una lengua desde cero a estos niveles, pero es algo con lo que tengo menos experiencia estos días.

<details>
<summary><b>Filosofía general</b></summary>
<div class="details">

Para aprender una nueva lengua parece que es fundamental la consumición de material comprensible en la lengua objetivo[^fn:1]. La **mayoría de mis esfuerzos se dirigen a ayudarme a consumir la mayor cantidad de material**, de la mayor complejidad posible.

Como no soy un robot, no me vale con torturarme 24 horas al día en un entorno de inmersión completa si no estoy preparado para ello, aparte de que aunque no dudo que eso funcionaría, probablemente no sea la forma más efectiva (entre otras cosas porque la probabilidad de que me queme es muy grande en este caso).

Así pues, la mayoría de mis esfuerzos se basan básicamente en:

-   conseguir la mayor cantidad de vocabulario pasivo que pueda para poder entender con facilidad;
-   estudiar gramática para poder entender las estructuras complicadas que vea antes de que me resulten naturales e intentar exponerme a ellas lo máximo posible;
-   minimizar el esfuerzo de los puntos anteriores y claro
-   aumentar en la medida de lo posible mi capacidad y facilidad para encontrar y disfrutar de material en la lengua objetivo.

Esto no significa abandonar los esfuerzos de producción en la lengua objetivo (es decir, no solo es importante leer y escuchar, también lo es hablar y escribir). Cuanto mayor es el nivel, más importancia tiene, pero en mi experiencia propia, una vez superada la vergüenza/el miedo de intentar hablar en otra lengua[^fn:2], en mi experiencia

-   la capacidad de comprensión es más importante que la capacidad de producción (si no entiendo lo que me dicen es mayor problema que si no me entienden en un principio);
-   el vocabulario activo (las palabras que puedo decir) sigue del vocabulario pasivo (las palabras que entiendo) incluso en casos en que activamente no lo estudio, es decir, que mucho del vocabulario que entiendo pero no puedo usar pasado un tiempo de entenderlo paso a poder usarlo, incluso si no hago un esfuerzo específico;
-   y además, darme tiempo a estar cómodo con la consumición de material antes de forzarme a mucha producción hace que el proceso sea más agradable y supone menos esfuerzo y disciplina.

Por supuesto, ser capaz de hablar y escribir en otra lengua también significa que puedo multiplicar la cantidad de exposición a la lengua natural... simplemente hablando o escribiendo con nativos u otra gente que la hable. También es parte del objetivo final -- si aprendo una lengua es para usarla activamente, generalmente, no solo para ver vídeos de Youtube en esa lengua --.
</div>
</details>


## Repaso espaciado {#repaso-espaciado}

Una de las herramientas con las que paso más tiempo es [Anki](https://apps.ankiweb.net/). Anki es una aplicación (disponible en web, Android e iPhone&nbsp;[^fn:3]) para ayudar a recordar cosas por medio de _tarjetas_. Una vez generadas estas tarjetas, nos «examinamos» (ver un ejemplo de cómo parece más abajo) periódicamente de una forma que está pensada para intentar maximizar la probabilidad de recordar el contenido.

<details>
<summary><b>El efecto <i>test</i> y el repaso espaciado</b></summary>
<div class="details">

El [**efecto _test_**](https://en.wikipedia.org/wiki/Testing_effect) es la observación, confirmada luego por psicólogos en distintos estudios, que la memorización de cosas se beneficia de intentar recordarlas, en vez de simplemente releerlas o volver a escucharlas. Es decir, el hecho de _testear_ ayuda a memorizar en sí.

Aparte, el [repaso espaciado](https://es.wikipedia.org/wiki/Repaso_espaciado) o la repetición espaciada, es una aplicación del efecto anterior, procurando incrementar el tiempo entre pruebas, buscando maximizar la eficiencia de la memorización. Por ejemplo, si quiero recordar, por ejemplo, que el verdadero 42º. presidente de los estados unidos fue, claramente, [Thomas J. Whitmore](https://es.wikipedia.org/wiki/Thomas_J._Whitmore), entonces tras leer este hecho, me «examino» de él un par de veces hoy para confirmar que me lo sé. Luego mañana de nuevo pruebo. Si acierto, igual en 3 días; si vuelvo a hacertar 5 días más tarde, luego 10... y así siempre incrementando el tiempo. En caso de fallar, empezamos con los intervalos de nuevo.

Hay aplicaciones que facilitan estas técnicas, y como vemos, Anki es una de ellas.

No está claro de qué intervalos son óptimos -- y algo de duda de que los intervalos sean incluso necesarios --, pero mi parecer es que intentar optimizar demasiado esto es algo a lo que dedicarse si tenemos demasiado tiempo en nuestras manos. En mi experiencia con la configuración por defecto (o alguna otra de las más conocidas) ya funcionan lo suficientemente bien, al menos para mí.
</div>
</details>

<aside>

<details>
<summary><b>Alternativas a Anki</b></summary>
<div class="details">

Anki me gusta por muchos motivos -- es muy flexible y es software libre --. Sin embargo, hay alternativas que pueden ser interesantes ya sea porque son más baratas (en iPhones), menos complicadas, más bonitas, o por lo que sea.

Lo primero es hablar en contra de una alternativa. Para iPhone hay una mala aplicación llamada AnkiApp (que no enlazo adrede). La empresa que hay detrás usa la fama de Anki para vender su propia alternativa que al parecer no es muy buena y no está relacionada con Anki. Mi recomendación es evitarles.

Y con esto, algunas alternativas:

[Memrise](https://www.memrise.com/)
: Memrise es prácticamente lo mismo, más bonito y menos complicado, pero con la pesadez de que cada día te intentan convencer para que pagues un plan profesional o algo. No hace falta y la aplicación gratis va bastante bien, y suele ser mi primera recomendación. Hay muchas colecciones de tarjetas de buena calidad para muchos idiomas (y más).

[Quizlet](https://quizlet.com/en-gb)
: Es muy parecido a Memrise, en que es una alternativa con una empresa comercial detrás que intenta convencerte para que pagues pero se puede usar gratis. No la he usado extensamente, pero muchos estudiantes de idiomas la usan y están contentos.

[Supermemo](https://supermemo.store/products/supermemo-18-for-windows)
: No quiero meterme en guerras religiosas, pero para algunos la mejor alternativa. El algoritmo de intervalos entre repeticiones de Anki viene de una versión anterior de Supermemo, y mucho de la popularización de los sistema de repaso espaciado vienen del autor de Supermemo. Supermemo facilita algunas cosas (como lo que llaman _aprendizaje incremental_) y si alguien realmente se aburre o quiere probar mucho con estos sistemas le animo a probar si quiere. Tienen también otro producto enfocado a idiomas, con colecciones de tarjetas ya creadas, pero sé poco de cómo va.
</div>
</details>

</aside>

¿Para qué uso Anki? Al fin y al cabo es una herramienta que facilita memorizar, pero memorizar no es suficiente para aprender un idioma, aparte de que memorizar por memorizar si atender a cómo se hace lo vuelve mucho más difícil. Lo primero es el qué intentar retener. Yo, en el contexto de lenguas, lo uso para todo:

-   Para empezar, y principalmente, para vocabulario (con las indicaciones que comento en el párrafo siguiente).
-   También para exponerme a formas gramaticales nuevas, especialmente si en un primer momento no me resultan naturales, es decir, si incluso traduciendo la oración me cuesta entenderla. Generalmente esto lo hago con tarjetas que piden traducir una frase (no tanto preguntando por la teoría que hay detrás).
-   La fonética si no la tengo clara.

Hasta ahora no he usado Anki, con idiomas, para otras cuestiones, pero probablemente también valga bien para ello.

Eso sí, cualquier tarjeta, como indicaba, no funcionará muy bien. Una tarjeta que simplemente diga `ausziehen ⇔ mudarse a` no será muy buena, por diversos motivos. Para empezar, muchas palabras tienen distintos significados o matices específicos (_ausziehen_ tiene ambos, como lo tiene el verbo _mudarse_ y claro, no coinciden demasiado), y además la palabra puede tener distintas formas -- en este caso, siendo verbos, con lenguas europeas generalmente hay unas cuantas formas al menos que terminar memorizando --. Para continuar, si queremos facilitar recordarlo, se ha visto que ayuda

-   conectar las tarjetas con cosas que ya sabemos o que hemos experimentado,
-   mezclar formas/medios (imágenes, texto, audio),
-   limitar la cantidad de información nueva en una tarjeta,
-   y bueno, otras recomendaciones que pueden ser importantes, pero con las anteriores ya tiene uno un buen comienzo.

En general hacer tarjetas uno mismo significa que la retención será más fácil sobre todo si las relacionamos con cosas personales, pero mucha gente utiliza tarjetas escritas por otros y les va bien (y se ahorra el tener que escribir todas las tarjetas claro). Para estudio de lenguas, sin embargo, diría que es muy importante aprender todo con contexto. Por ejemplo, con vocabulario limitar o evitar recordar una traducción para una palabra, sino recordar frases que incluyen el nuevo vocabulario. Siguiendo el ejemplo anterior, sería mejor tener unas cuantas tarjetas estilo `„Wenn ich 20 bin, ziehe ich aus dieser Bruchbude aus.“ ⇔ «Cuando tenga 20, me mudaré de este cuchitril.»`, `„Die Mutter legte das Baby auf den Wickeltisch und zog es aus.“ ⇔ «La madre puso al bebé sobre el cambiador y lo desvistió.»` y otras similares, siendo la palabra nueva la parte de la frase que es una verdadera novedad.

Anki puede ser una herramienta muy potente y eficiente. Actualmente tengo miles de tarjetas y usándolo noto que avanzo bastante rápido memorizando vocabulario (pasivo), aceptando nuevas formas gramaticales y ayudándome a no destrozar demasiado las lenguas cuando intento hablarla. Sin embargo, no deja de ser una herramienta para memorizar, y esto no es suficiente para poder llegar a hablar con fluidez una lengua.

Una cosa que no he comentado asumiendo que está claro, pero que no está de mal recalcar es que antes de acercarse a ninguna tarjeta o sistema de memorización, hay que aprender. Memorizar funciona bien cuando uno ya tiene algo, conceptos e ideas, a los que conectar la nueva información, y cuando podemos entenderla. Especialmente al principio, cursos de distinto tipo ayudan aquí; pero eventualmente con niveles más altos las tarjetas pueden «surgir» de forma más natural. En las siguientes secciones indico algunas posibles fuentes de tarjetas (aparte del uso propio de la herramienta), aunque no dejo de consultar gramáticas, diccionarios y otras herramientas cuando las genero para entender bien el nuevo concepto.


### ¿Cómo es usar Anki? {#cómo-es-usar-anki}

Esto no es un tutorial, y hay muchísimos en otras páginas (generalmente más cortos que esta página ;) ), pero unas cuantas imágenes y comentarios para mostrar cómo es.


#### Las pruebas periódicas {#las-pruebas-periódicas}

Cada día abro Anki, en el móvil, o en el ordenador (a veces en la web) y me encuentro con
![](/ox-hugo/anki-main.png)

Ahí hay varias colecciones (y subcolecciones) que tengo. Elijo una de ellas y marco que quiero ponerme a prueba. Me sale entonces la primera tarjeta, por ejemplo:
![](/ox-hugo/anki-question.png)

Leo la pregunta, intento acordarme de la respuesta y cuando ya creo que la tengo, o me canso de intentar recordar, le doy a mostrar respuesta:
![](/ox-hugo/anki-answer.png)

Si acierto, marco que acerté. Si no, le doy a repetir.

Las imágenes están con la interfaz en inglés porque lo tengo así, pero Anki está completamente traducido al castellano (y alemán y otras lenguas). El contenido de la tarjeta, por cierto, es como yo he ido haciéndolo, pero igual mejor no tomarlo como ejemplo a seguir.


#### Conseguir nuevas colecciones {#conseguir-nuevas-colecciones}

Anki, _online_, ofrece muchas colecciones para descargar de muchos idiomas y de muchos otros temas. Generando una cuenta gratuita de Anki, además, podremos sincronizar nuestras colecciones _online_ y con todos nuestros dispositivos (así es como uso las mismas tarjetas en el ordenador y en el móvil).


#### Generar y editar nuevas tarjetas {#generar-y-editar-nuevas-tarjetas}

Anki permite generar nuevas tarjetas fácilmente y proporciona muchas facilidades para ello. Además, hay muchas extensiones que se pueden instalar para hacer la vida aún más fácil. Si somos algo manitas y tenemos tiempo, o si tenemos conocimientos informáticos avanzados, también podemos usar otras herramientas para añadir o editar los contenidos de las tarjetas. Yo generalmente creo las tarjetas en un editor aparte porque me es más cómodo, y porque según el tema puedo compaginar con otro tipo de notas, pero la mayoría de usuarios probablemente estén más que bien servidos con el sistema de edición por defecto.


## Consumición libre {#consumición-libre}

**O: leer y escuchar según me interesa.**

La mayoría de mi «consumición» (es decir, leer o escuchar) en la lengua objetivo la intento hacer por gusto. Esto me parece importante para intentar disfrutar la lengua, no centrarme solo en estudio, y además poder ir aprendiendo cosas sobre la cultura de la lengua. Una lengua es parte de la cultura, y saber de la(s) cultura(s) es importante para aprender efectivamente la lengua.

Para ello si encuentro material adecuado, escucho _pódcasts_, veo vídeos en Youtube y similares, series y películas con subtítulos (¡o sin subtítulos si puedo!), intento leer libros o cuentos, y busco foros de internet de temas que me interesen. Por supuesto, todo esto depende mucho de la lengua que uno estudie y los intereses personales, pero básicamente es intentar entretenerse y hacer cosas que normalmente haría de todas formas, pero en la lengua objetivo. Cuando consumo este material siempre hay bastante que no entiendo completamente. Si estoy muy perdido, entonces busco otra cosa, no es entonces algo provechoso (o lo estudio como indico en el siguiente apartado), pero si no, con tal de que pueda enterarme en general o poder disfrutarlo, me contento con eso. No hago un esfuerzo para buscar cosas específicas salvo que sean muy puntuales ni lo afronto como si fuera estudio.

Por supuesto, según la lengua y los intereses, encontrar este material puede ser complicado, al menos hasta conseguir un nivel relativamente avanzado. Libros infantiles[^fn:4] u obras creadas para estudiantes de la lengua, si están disponibles, pueden ser las primeras cosas que buscar. Otra opción es re-leer, re-ver, o re-escuchar cosas que ya hemos disfrutado (en nuestra lengua u alguna otra que conozcamos bien) y que no nos importa repetir. En este caso ya tenemos el contexto más claro y perdernos es más difícil. Además, ya sabemos que el material nos gusta ;) .


## Consumición «controlada» {#consumición-controlada}

**O: leer y escuchar con ayudas / leer y escuchar como estudio activo**

Aparte de la consumición libre, algunas obras las uso como medio de estudio. Ello no significa que cada palabra y frase las estudie con detalle, pero sí que generalmente iré con ella con más cuidado y suelo repasarlas varias veces.


### Libros digitales {#libros-digitales}

Algo que no hago mucho, pero sí hice bastante en su momento y mucha gente aprovecha bastante, es usar lectores digitales para intentar leer en otros idiomas. Estos dispositivos suelen tener diccionarios integrados que permiten «pinchar» una palabra y obtener una traducción (o definición si podemos usar un diccionario monolingüe). Esto permite ir leyendo poco a poco y obtener traducciones de palabras específicas sobre la marcha.

Esto funciona muy bien si uno tiene nivel suficiente para no tener problemas con la estructura de la lengua o referencias culturales. Como medio de estudio no me parece ideal, pero como consumición libre (es decir solo para disfrutar) sí me parece bien. De otra forma me parece que es algo pesado ir palabra por palabra si es un texto que no podemos controlar bien.


### Libros físicos {#libros-físicos}

Algo que disfruto más es leer libros físicos. Suelo hacerlo en varias fases. Algunas de estas fases a veces las junto o las ignoro completamente, pero por simplificar pongo el proceso más largo aquí. Entre cada fase puede pasar un tiempo:

1.  Leo primero intentando entender tanto como puedo.
2.  Leo de nuevo, ahora más lento y marcando las partes que no entiendo bien. Es posible que alguna cosa la busque sobre la marcha si pienso que hace falta, pero normalmente aquí solo marco (subrayo con lápiz). Marco palabras, expresiones, frases, a veces hasta párrafos si creo que hace falta. No marco todo lo que no entiendo entero, solo aquellas cosas que parecen ser más necesarias para entender mejor, o simplemente por las que tengo curiosidad.
3.  Luego voy por las marcas subrayadas y las busco. Generalmente creo notas de Anki sobre ellas, aprovechando el contexto que me da el libro.
4.  Un tiempo más adelante lee de nuevo, en teoría entiendo mejor.

<details>
<summary>Fotos de ejemplo</summary>
<div class="details">

Pincha en las imágenes para verlas en mayor calidad en otra pestaña. Sí, son páginas con algunos subrayados, no hay nada sorprendente ;) .

{{< figure src="/ox-hugo/underline-small-german.jpg" target="_blank" link="/ox-hugo/underline-big-german.jpg" >}}

{{< figure src="/ox-hugo/underline-small-portuguese.jpg" target="_blank" link="/ox-hugo/underline-big-portuguese.jpg" >}}
</div>
</details>

Con esto hay varios objetivos:

-   Lo primero es que es una fuente para aprender nuevas palabras, estructuras, expresiones, lo que sea, en un contexto que conozco.
-   Además, cada vez que re-leo me fijo en cosas nuevas que antes no me había fijado, en parte porque entiendo el texto mejor.
-   Y de paso mi cerebro va procesando todo esto (en contexto, además, que es bueno) y ayudando a formar el modelo del lenguaje objetivo.

Puede parecer pesado, pero la verdad me gusta bastante. Por supuesto, busco libros/revistas que me interesen, y el tiempo que dedico a ello es tiempo de estudio. No me engaño pensando que será tiempo de lectura, porque en ese caso al menos yo dejo de estudiar.


### Lute (o similares) {#lute--o-similares}

Una alternativa a lo anterior, es usar un programa para hacer el mismo proceso. Lo uso con algunas noticias complicadas, artículos largos y más (ver la sección siguiente).

<aside>

<details>
<summary><b>Alternativas a Lute</b></summary>
<div class="details">

[LingQ](https://www.lingq.com/es/)
: La versión más profesional, de pago. No solo es más cómoda de usar, tiene bastantes funcionalidades, incluyendo pudiendo usarse con vídeos y audios. Algunas de las otras alternativas que hay aquí (y de forma indirecta Lute) se crearon como alternativas más baratas/gratis/libres de LingQ.

[LWT](https://learning-with-texts.sourceforge.io/)
: Otra versión libre del mismo concepto que Lute, emulando las funcionalidades principales de LingQ. Lute se creó como una versión más moderna de LWT, y cada uno tiene funcionalidades que el otro no tiene. También requiere conocimientos técnicos bastante avanzados para instalarse.

Otros
: Hay unas cuantas alternativas más que funcionan de forma similar. Abajo hablo brevemente de otras dos, [LanguageReactor](https://www.languagereactor.com/) y [Readlang](https://readlang.com/), que son similares pero con suficientes diferencias como para que cuando las he usado las he usado de formas bastante distintas.
</div>
</details>

</aside>

Básicamente los pasos que indico en el punto anterior los sigo igual, pero usando una herramienta que facilita parte de ellos. La herramienta que yo uso es [Lute](https://github.com/jzohrab/lute), y requiere conocimientos técnicos avanzados para poder instalarlo, pero hay alternativas bastante buenas que comento en el bloque de alternativas. De hecho, probablemente es recomendable para la mayoría de la gente probar una de las alternativas, que será más fácil de usar (y será menos feo).

En la herramienta hay que añadir textos, cosa que es sencilla una vez se sabe como, y luego se abre y tiene un aspecto como este:
![](/ox-hugo/lute.png)

Ahí hay 3 partes. A la izquierda vemos el texto que vamos leyendo. Algunas palabras están con un fondo coloreado[^fn:5]:

<span style="color: black; background: #f8f8f8;" title="blanco (sin fondo)">blanco (sin fondo)</span>
: esto son palabras que ya han sido marcadas como conocidas, o que deberían ignorarse;

<span style="color: black; background: #D5FFFF;" title="azul clarito">azul clarito</span>
: esto son palabras que no están registradas aún;

<span style="color: black; background: #F5B8A9;" title="algún otro color">algún</span><span style="color: black; background: #F5CCA9;" title="algún otro color"> </span><span style="color: black; background: #F5E1A9;" title="algún otro color">otro</span><span style="color: black; background: #F5F3A9;" title="algún otro color"> </span><span style="color: black; background: #DDFFDD;" title="algún otro color">color</span>
: esto son palabras que están en proceso de aprendizaje (el color específico depende cuánto se marca que se sabe la palabra)

Cuando clicas en una palabra, a la arriba a la izquierda puedes poner una definición (o volver a encontrarla). Si una palabra ya tiene una definición sale cuando pones el cursor por encima de la palabra.

Abajo a la derecha, además, te sale un diccionario con la palabra para facilitar el proceso. Hay más facilidades que no comento, como que si con el cursor encima de una palabra pulsamos una tecla (una `t`) se abre una ventana del navegador con un traductor automático _online_ con la frase entera, y otras cosas por el estilo.

Lute está pensado para luego registrar todas las palabras que uno va aprendiendo e incluso testearlas (como Anki) o exportar incluso a Anki. Yo no lo uso así la verdad, tengo un proceso similar al de los libros en que lo hago más manual, pero la idea no es mala y algunos productos comerciales se basan en esto como la principal forma de aprendizaje. A mí me sirve para de forma sencilla cargar textos y poder repasarlos con poco esfuerzo y más rápido que si lo hiciera todo a mano. Cuando he repasado un texto lo suficiente, lo archivo y voy a otra cosa. A veces, como decía, alguna noticia o artículo que me resulta difícil de leer por el vocabulario o por las expresiones (Lute permite registrar también expresiones[^fn:6]) hago una pasada rápida en la que lo uso como «diccionario rápido», y con ello leo igual el texto en el idioma que quiero, pero sin demasiado esfuerzo por las partes complicadas (en contraposición con traducir automáticamente todo el texto de la noticia usando Google Translate o DeepL).


### Lute, Whisper y pódcasts {#lute-whisper-y-pódcasts}

Los ejemplos que he comentado son sobre textos escritos tanto físicos como digitales. Alguna de las alternativas a Lute permiten usarse con vídeos o audio de forma similar, pero yo prefiero otro proceso. Con algunos [pódcasts](https://es.wikipedia.org/wiki/P%C3%B3dcast), el proceso que sigo es básicamente el mismo que con los textos digitales:

1.  Escucho el pódcast e intento entender todo lo que pueda.
2.  Consigo la grabación del pódcast y extraigo la transcripción con Whisper (debajo explico qué es).
3.  Añado la transcripción como texto en Lute, y la leo independientemente del pódcast. Repaso las partes y palabras que no entiendo.
4.  Igual añado tarjetas a Anki y empiezo a repasarlas.
5.  Vuelvo a escuchar el pódcast leyendo la transcripción.
6.  (Si tengo muchas ganas) vuelvo a escuchar el pódcast sin leer nada.

[Whisper](https://openai.com/research/whisper) es un programa que si se le pasa una grabación de audio o vídeo puede sacar la transcripción, y generalmente será bastante buena. No es del todo perfecta... pero mejor que yo desde luego, para las lenguas que intento aprender. Es sorprendentemente buena. [Instalarlo es complicado y requiere buenos conocimientos técnicos](https://blog.castopod.org/transcribe-your-p%C3%B3dcast-for-free-using-only-a-laptop-and-whisper/), y para funcionar bien requiere un ordenador con una buena tarjeta gráfica, eso sí. Por suerte, algunas de las alternativas de Lute hacen que no haga falta depender de ello. Y en el futuro me puedo esperar que haya servicios que ofrezcan esto de forma más sencilla (si no los hay ya claro; sé que hay ofertas comerciales para esto, pero no he probado ninguna la verdad).


### Otros medios {#otros-medios}

Esto que en las secciones anteriores hemos visto, lo podemos tener bastante similar con vídeo (y audio sin el esfuerzo de transcribir). Yo no lo uso mucho últimamente, aunque sí en el pasado, ya que prefiero simplemente ver algo con subtítulos y tirar a partir de ello. Sin embargo, hay algunas herramientas que he usado que querría mencionar aún así.

[LanguageReactor](https://www.languagereactor.com/) originalmente se llamaba _Language Learning with Netflix_ (aprendiendo idiomas con Netflix). Tiene muchas funcionalidades distintas, incluyendo un modo de repaso/estudio/lectura de textos similar a como funciona Lute, pero su principal atractivo es poder ver vídeos de Netflix o Youtube con subtítulos en la lengua que aprendemos y en la propia. Además, permite pinchar en palabras, ver el significado específico, guardarla y más. En general funciona bastante bien, y la razón principal por la que no lo uso ya demasiado es porque para vídeos me cansa tanta información. Prefiero, como comentaba, ver vídeos normales con subtítulos y probar lo mejor posible, pero no tanto como herramienta de estudio activo.

Una alternativa que usé en su momento bastante también es [Readlang](https://readlang.com/). Readlang funciona también similar a Lute con textos, pero permite sincronizar estos con vídeos de Youtube. En su momento lo usé bastante para practicar mi oído escuchando canciones (hay bastantes en las lenguas que probé ya subidos).

También debería volver a mencionar [LingQ](https://www.lingq.com/es/) (de las alternativas a Lute, arriba). El producto tiene vídeos y audios con los que practicar, al menos en alguno de los idiomas.


## Producción {#producción}

**O: hablar y escribir**

Tras tanto que decir sobre leer y escuchar, sobre escribir y hablar tengo menos. Lo más sencillo, y quizá lo mejor, es buscar nativos con paciencia con los que poder practicar. Las herramientas tradicionales para buscar compañero de _tándem_ (un hablante de otra lengua con quien quedas para un rato hablar una lengua y luego otro rato la otra, generalmente) nunca me han funcionado muy bien, pero he podido encontrar gente con quien practicar (o a quien escribirme o lo que sea).

<aside>

Los modelos grandes de lenguaje son cosas como ChatGPT, las inteligencias artificiales que simulan una persona real y que suelen responder preguntas. Pueden ser muy útiles pero nunca 100% fiables.

</aside>

Si queremos probar otras cosas o nos cuenta encontrar gente, para escribir hay varias opciones. Para algunas lenguas hay páginas, como los distintos [_subreddits_ de _WriteStreak_](https://www.reddit.com/r/WriteStreak/)[^fn:7] en los que la gente manda textos cortos escritos y un nativo los corrige. Por lo que veo, en la mayoría de los idiomas no hay demasiados nativos corrigiendo, pero puede ser buena práctica. Otra opción es hablar directamente con [ChatGPT](https://chat.openai.com/) en el idioma que queramos. Generalmente funcionará bastante bien, pero eso sí, como todo lo que esté relacionado con modelos grandes de lenguaje/<abbr title="Large Language Model">LLM</abbr>s hay que tener bastante cuidado, especialmente con lenguas menos comunes. Aprovechando ChatGPT, hay opciones como [PrettyPolly](https://www.prettypolly.app/) que permiten mantener una conversación en algunos idiomas.


## Ayudas adicionales {#ayudas-adicionales}

Hay por supuesto muchísimas más herramientas que pueden ser útiles y algunas las he usado o uso incluso hoy día.

[Duolingo](https://www.duolingo.com/) es probablemente la aplicación de aprendizaje de idiomas más conocida. Es muy buena consiguiendo que uno se sienta a obligado a hacer la tarea diaria, y ayuda a tener contacto con el idioma. Como ayuda adicional creo que puede funcionar bastante bien, pero sí que no debería ser la única herramienta que usamos, y como mínimo debería ser complementada con algo más. Es gratuita, y el búho sabe presionarte emocionalmente como ninguna otra persona, animal o cosa podría hacerlo.

[Babbel](https://es.babbel.com/) ofrece cursos de algunos idiomas. Es de pago (bueno, creo que hay una lección gratis, pero eso no te da para nada, aunque sí puedes ver cómo es), pero si esperas un poco suelen tener buenas ofertas. Tengo que decir que lo usé unas cuantas veces y funcionó sorprendentemente bien para alemán, aunque era originalmente escéptico. Algunas lenguas, eso sí, tienen contenidos más limitados (en holandés había algo, pero desde luego no como en alemán). [Busuu](https://www.busuu.com/es) se propone como alternativa a Babbel a veces. Se puede pagar, pero tiene bastante material gratis. La uso a veces para dar algún tema, y no es mala para eso, aunque la gestión de vocabulario sí creo que es bastante mala.

En la sección de cómo empezar hablo de [LanguageTransfer](https://www.languagetransfer.org/), que puede merecer la pena incluso si uno ya tiene cierta idea en una lengua. Y por supuesto, hay muchísimas otras opciones ([Michel Thomas](https://www.michelthomas.com/) sería la opción de pago por ejemplo de algo como LanguageTransfer, con acceso a más lenguas y quizá material). Muchas de estas pueden funcionar bien como base del idioma (algo de fonética, gramática y base de vocabulario), pero su eficiencia depende mucho también de la lengua.

Aparte, no hay que olvidar herramientas específicas para algunas lenguas, que suele haber bastantes. Por dar un ejemplo, en alemán hay muchos posibles artículos (el, la, lo, los, las, un, una, unos, unas) que dependen de género, función en la frase, número... Es algo importante para poder hablar y entender bien el idioma. [Superanto](https://chrome.google.com/webstore/detail/superanto/dnkneahdphegebpadfnaijejemnmcmpe) es una extensión de Chrome, si usamos ese navegador, que puede quitar todos los artículos de una página en alemán y nos permite ponernos a prueba seleccionando el artículo que correspondería y viendo si es correcto.


## Cómo empezar {#cómo-empezar}

Si empezamos de 0 en una lengua, muchas de las opciones anteriores pueden ser un poco demasiado al principio. En ese caso, un curso tradicional de introducción puede ser buena idea.

Si queremos ir por nuestra cuenta, de todas formas, unas opciones son:

-   Lo primero, si se escribe en un alfabeto o silabario distinto, aprenderlo. Esto no suele costar mucho[^fn:8] y hará la vida mucho más fácil más adelante.
-   Si hay un curso de [LanguageTransfer](https://www.languagetransfer.org/) (alternativamente, Michel Thomas, Pimsleur...) disponible, empezar con él.
-   Para muchos idiomas Duolingo y aplicaciones similares como apoyo pueden ayudar[^fn:9]
-   Cuando ya se tiene algo de idea con la lengua, podemos empezar a usar sistema de repaso espaciado (Anki, Memrise o lo que prefiramos y que nos vaya bien).
-   Desde pronto es importante intentar exponernos al idioma, buscando material sencillo que podamos entender algo, peor también con música y otras formas de poder disfrutar la lengua sin que sea abrumador.
-   Ayuda igual buscar temarios de cursos de lenguas, si no seguimos ninguno y queremos hacerlo todo por nuestra cuenta, para ir viendo qué cosas conviene ir repasando eventualmente. Prácticamente todo el que termina aprendiendo una lengua lo hace en un orden similar. No quiere decir que este orden sea el de los cursos, pero con suerte la mayoría de los cursos sí va por un camino que alguien que está aprendiendo puede seguir.
-   Sobre todo, buscar motivación energía y ganas, por que aprender un idioma es algo bonito y un nuevo mundo, pero es también duro y bastante esfuerzo. Merece la pena y no hay que quemarse nada más empezar, con tiempo se puede todo, pero hay que ser consciente de ello :) .

[^fn:1]: Esto parece claro, tanto para la mayoría de la gente (al menos con las que yo hablo ;) ), [como para los expertos](https://es.wikipedia.org/wiki/Adquisici%C3%B3n_de_segunda_lengua). A partir de aquí casi es cuando empiezan a no haber tanto consenso. Algunos expertos piensan que [lo único fundamental es la consumición de material](https://en.wikipedia.org/wiki/Input_hypothesis) -- y por tanto aprendemos nuevas lenguas de adulto de forma similar a la primera lengua --, y otros creen que, como mínimo, esta es una forma no demasiado eficiente.

    En la práctica las diferencias pueden importar a nivel teórico y práctico. Para mí lo que está claro es que consumir mucho material es importante, y en mi experiencia algunas técnicas tradicionales de aprendizaje de idiomas (como cierta cantidad de estudio de gramática, quizás en un contexto de aula bien organizado) pueden ayudar con esto.
[^fn:2]: Cosa que por suerte es algo con lo que personalmente no tengo tanto problema.
[^fn:3]: Aunque la versión de iPhone es de pago -- la única que lo es, para financiar el desarollo --. La recomiendo igual, aunque para mí es fácil de decir cuando no pago. Hay alternativas en el bloque al lado del texto que pueden también valer.
[^fn:4]: Quizá no muy antiguos. Actualmente estoy leyendo un libro que leen los colegiales alemanes en el colegio, escrito hace unos 100 años («Das fliegende Klassenzimmer»). El lenguaje usado en el libro es bastante poético y parte del lenguaje (o de las figuras literarias) son un poco anticuados. Un amigo alemán bromeó que si sigo terminaré hablando alemán de otro siglo. La experiencia no es mala... pero con libros u otras obras modernas este tipo de cosas no deberían de pasar tanto.
[^fn:5]: Como soy daltónico, usé una página web para buscar el nombre de los colores, ¡espero que sean correctos!
[^fn:6]: Lute y herramientas similares no son perfectas, claro. Un ejemplo de algo con lo que no funcionan tan bien es con expresiones que no son consecutivas o con cosas como verbos separables -- algo que el alemán y holandés tienen por ejemplo -- en la que un verbo puede romperse a veces y aparecer desperdigado en distintas partes de la frase. En estos casos Lute no puede saber que esas dos cosas separadas en teoría son la misma «palabra». Aún así para todo (al menos que yo haya visto) hay formas de minimizar los problemas o las faltas.
[^fn:7]: El enlace manda a la versión para inglés, pero hay enlaces a otros _subreddits_ en otras lenguas.
[^fn:8]: Con lenguas como chino, aprender los caracteres es importante, pero desde luego no lo primero antes de nada más. Con japonés, hiragana y katakana ayudan. Y similar con otras lenguas.
[^fn:9]: Hago énfasis en como apoyo. Muchas veces incluso se puede empezar con Duolingo para empezar con un idioma, pero por ejemplo con gaélico irlandés hay muchas recomendaciones de **no** empezar con Duolingo porque puede confundir y llevar a errores y confusiones grandes pronto, y en mi experiencia estudiando un poco de la lengua, así es. No quita que como apoyo también pueda ser útil.
