+++
title = "Framework Laptop"
author = ["Abby Henríquez Tejera"]
categories = ["reviews", "hardware"]
draft = true
+++

-   screen (4:3)
-   battery
-

(tldr) After a month or so I'm quite happy with the laptop, with a couple of caveats.

Bad (mostly things related to Linux on the Framework):

-   Battery live is not great although this is not a problem for me. This said, I haven't spent that much time trying to optimise it.
-   I had to do a BIOS/UEFI conf. change to avoid freezes from happening.
-   Some people have complained about some background noise with the headphones. I haven't noticed it, but also haven´t used it a lot, and there seems to be solutions for this.

Neutral:

-   Metallic cover - most people consider this good. Some people have complained in the past about the cover, but it's gone well with me.
-   The keyboard is quite good (I'm using the ISO layout btw, but you can choose) for a laptop keyboard - but of course this will be subjective.
-   Bought the half terabyte hard drive, but would now take a bigger one (still 60% free though - and a big percentage is always occupied for Nix).
-   The looks are not great, but not horrible. I find the XPS _prettier_, and they are also slimmer. This is though, also subjective (well, being smaller/slimmer not, but the difference is not that big).

Great:

-   Well, in general everything else is quite good. I really like the plugin extension system for input ports. Haven't used it a lot, I mostly just use USB/USB-C so no need to use a lot the others, but it's nice to have them.
-   Support seems responsive, and in the forums and discord people don't seem toxic (or much less than in other similar communities).
-   Haven't had to do any repairs, but it all looks easy to do even for me, and the assembly was piece of cake (and I'm  the highest level clumsy possible).

Works quite well for me - but also I'm comparing with a 6 year old laptop. In comparison with the old XPS, well, there's slight less Linux support as of now - supermodern hardware vs very old hardware - but much more than there was when I got the XPS, actually giving quite a good experience with Linux. As said, it's slightly bigger and to me looks a bit worse to the eye but not enough to care. I care that it compiles stuff faster and goes quite smooth (6 years of new CPU tech is worth something I guess). The keyboard layout and the camera position are **much** better with the Framework.

Comparing with the Carbon X1 that I had from work... it's quite similar. I must admit (argh) that I prefer the external aesthetics and feeling of the Carbon, but not enought to get that instead of the Framework (also, got a sticker for the Framework in any case). The battery life was a bit better with the Carbon (and the same system, so it's a good comparison), but the Carbon had a distinct lack of USBC ports for me which was annoying. Compiling time and general usage... very similar (or the same) in both laptops. It should be faster with my Framework, but it's not directly noticeable as with the 2016 XPS (one year or so of CPU tech difference is not that much ;) ).

If you have any questions, happy to take them :) .
