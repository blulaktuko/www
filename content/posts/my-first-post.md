+++
title = "My first post"
author = ["Abby Henríquez Tejera"]
date = 2022-10-08T17:41:00+02:00
lastmod = 2022-10-09T22:12:00+02:00
tags = ["internal"]
categories = ["internal"]
draft = false
weight = 0
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2022-10-09T22:12:00+02:00
      note = "Added an aside with notes to the post-meta. This is a longish note to see how it goes. It includes an emoji 😼."
    [[logbook._toplevel.notes]]
      timestamp = 2022-10-09T00:31:00+02:00
      note = "Added some extra code"
    [[logbook._toplevel.notes]]
      timestamp = 2022-10-08T20:33:00+02:00
      note = "Added new stuff"
+++

This is a random post where I try stuff for the blog.

<!--more-->

```text
This is an example
```

> This is a quote

<aside>

Some content for the `aside` block.

</aside>
<mark>This sentence is marked.</mark>


## Code blocks {#code-blocks}

Hy:

```hy
(defn get-digest [file-path]
  (setv hash (.sha1 hashlib))
  (setv block-size (. hash block_size))
  (with [f (open file-path "rb")]
    (reduce (fn [acc chunk]
              (.update hash chunk))
            (iter (fn [] (.read f block-size))
                  b"")
            hash))
  (.hexdigest hash))
```

Haskell:

```haskell { linenos=true, linenostart=1, hl_lines=["3"] }
primes = filterPrime [2..]
  where filterPrime (p:xs) =
          p : filterPrime [x | x <- xs, x `mod` p /= 0]
```


## Continuing {#continuing}

Elisp:

```emacs-lisp { linenos=true, linenostart=4 }
(let ((files (directory-files-recursively "content-org/" "\\.org$")))
  (dolist (file files)
          (save-excursion
            (with-current-buffer (find-file file)
              (org-hugo-export-wim-to-md :all-subtrees nil nil :noerrors)))))
```

Here `monospace` text. `C-c` is a keybinding.
