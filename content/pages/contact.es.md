+++
title = "Contacto"
author = ["Abby Henríquez Tejera"]
date = 2022-10-09T03:11:00+02:00
lastmod = 2023-08-15T09:24:00+02:00
categories = ["pages"]
draft = false
hidden = true
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2023-08-15T09:24:00+02:00
      note = "Añadida cuenta de Mastodon, eliminada cuenta de Twitter/X."
+++

Puedes ponerte en contacto conmigo por cualquiera de estos medios:

-   [abby@blulaktuko.net](mailto:abby@blulaktuko.net)
-   [github](https://github.com/paradoja/)
-   [gitlab](https://gitlab.com/paradoja/)
-   [mastodon](https://mastodon.social/@paradoja)
-   [linkedin](https://www.linkedin.com/in/abbyhenriquez/)
