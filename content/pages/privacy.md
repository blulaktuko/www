+++
title = "Privacy & data usage"
author = ["Abby Henríquez Tejera"]
date = 2022-10-09T03:41:00+02:00
categories = ["pages"]
draft = false
hidden = true
+++

No data is collected in this page directly (yep, no tracking, analytics or whatever).

As a hosting provider, [Netlify](https://www.netlify.com/) does have access to a tiny bit of information that they do store. It's not much and can be checked on their [privacy](https://www.netlify.com/privacy/) and [GDPR](https://www.netlify.com/gdpr-ccpa) pages. Basically, from visitors they get the access logs (which pages a specific IP visits and when; I am, though, not a lawyer).
